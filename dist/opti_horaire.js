/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/js/horaire.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/js/horaire.js":
/*!***************************!*\
  !*** ./src/js/horaire.js ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("/// HTML TARGET FOR APPENDCHILD ///\n\nlet linesTram = document.querySelector(\".lines-tram\");\nlet linesChrono = document.querySelector(\".lines-chrono\");\nlet linesProxi = document.querySelector(\".lines-proxi\");\nlet linesFlexo = document.querySelector(\".lines-flexo\");\nlet lineInfo = document.querySelector(\".line-info\");\nlet lineControls = document.querySelector(\".line-controls\");\nlet reverseDirect = document.getElementById(\"reverse\");\nlet lineTime = document.querySelector(\".line-time\");\n\n/// REQUETE INFOS ALL LINES ///\n$.ajax({\n    url: `http://data.metromobilite.fr/api/routers/default/index/routes`,\n    dataType: \"json\",\n}).done(function (data) {\n    \n    /// GET : TRAM, CHRONO, PROXIMO & FLEXO LINE INFO ///\n    for (let i = 0; i < data.length; i++) {\n        let info = data[i];\n\n        if (info.type === \"TRAM\" || info.type === \"CHRONO\") {\n\n            let lineNumber = document.createElement(\"div\"); // LINE NUMBER\n            lineNumber.classList.add(\"line-number-circle\");\n            lineNumber.style.backgroundColor = `#${info.color}`;\n            lineNumber.innerText = info.shortName;\n\n            if (info.type === \"CHRONO\") {\n                lineNumber.style.color = \"#000\";\n            }\n\n            /// EVENT LISTENER ON LINE NUMBER ///\n            lineNumber.addEventListener(\"click\", function () {\n\n                lineInfo.innerHTML = \"\"; // EMPTY LINE INFO\n                lineTime.innerHTML = \"\"; // EMPTY LINE TIME\n\n                let lineName = document.createElement(\"div\"); // LINE NAME\n                lineName.classList.add(\"line-name\");\n                lineName.style.backgroundColor = `#${info.color}`;\n                lineName.innerText = info.shortName;\n                lineInfo.appendChild(lineName);\n\n                let lineDirect = document.createElement(\"p\"); // LINE DIRECTION\n                lineDirect.classList.add(\"line-direct\")\n                lineDirect.innerText = info.longName;\n                lineInfo.appendChild(lineDirect);\n\n                lineControls.classList.remove(\"hide\");\n                lineControls.classList.add(\"show\");\n\n                /// REQUEST : INFOS SELECTED LINE ///\n                $.ajax({\n                    url: `http://data.metromobilite.fr/api/ficheHoraires/json?route=${info.id}`,\n                    dataType: \"json\",\n                }).done(function (data) {\n                    let time = data;\n\n                    let x = 0; // VALUE FOR CHOOSE LINE DIRECTION\n\n                    /// GET : EVERY STATIONS OF SELECTED LINE ///\n                    lineTableTime(time, x);\n\n                    reverseDirect.addEventListener(\"click\", function () {\n\n                        lineTime.innerHTML = \"\"; // EMPTY LINE TIME\n\n                        if (x === 0) {\n                            x = 1;\n\n                            /// GET : EVERY STATIONS OF SELECTED LINE ///\n                            lineTableTime(time, x);\n                        }\n                        else {\n                            x = 0;\n\n                            /// GET : EVERY STATIONS OF SELECTED LINE ///\n                            lineTableTime(time, x);\n                        }\n                    })\n                })\n            })\n\n            if (info.type === \"TRAM\") {\n                linesTram.appendChild(lineNumber);\n            }\n            else {\n                linesChrono.appendChild(lineNumber);\n            }\n        }\n\n        else if (info.type === \"PROXIMO\" || info.type === \"FLEXO\") {\n\n            let lineNumber = document.createElement(\"div\"); // LINE NUMBER\n            lineNumber.classList.add(\"line-number-square\");\n            lineNumber.style.backgroundColor = `#${info.color}`;\n            lineNumber.innerText = info.shortName;\n\n            /// EVENT LISTENER ON LINE NUMBER ///\n            lineNumber.addEventListener(\"click\", function () {\n\n                lineInfo.innerHTML = \"\"; // EMPTY LINE INFO\n                lineTime.innerHTML = \"\"; // EMPTY LINE TIME\n\n                let lineName = document.createElement(\"div\"); // LINE NAME\n                lineName.classList.add(\"line-name\");\n                lineName.style.backgroundColor = `#${info.color}`;\n                lineName.innerText = info.shortName;\n                lineInfo.appendChild(lineName);\n\n                let lineDirect = document.createElement(\"p\"); // LINE DIRECTION\n                lineDirect.classList.add(\"line-direct\")\n                lineDirect.innerText = info.longName;\n                lineInfo.appendChild(lineDirect);\n\n                /// REQUEST : INFOS SELECTED LINE ///\n                $.ajax({\n                    url: `http://data.metromobilite.fr/api/ficheHoraires/json?route=${info.id}`,\n                    dataType: \"json\",\n                }).done(function (data) {\n                    let time = data;\n\n                    let x = 0; // VALUE FOR CHOOSE LINE DIRECTION\n\n                    /// GET : EVERY STATIONS OF SELECTED LINE ///\n                    lineTableTime(time, x);\n\n                    reverseDirect.addEventListener(\"click\", function () {\n\n                        lineTime.innerHTML = \"\"; // EMPTY LINE TIME\n\n                        if (x === 0) {\n                            x = 1;\n\n                            /// GET : EVERY STATIONS OF SELECTED LINE ///\n                            lineTableTime(time, x);\n                        }\n                        else {\n                            x = 0;\n\n                            /// GET : EVERY STATIONS OF SELECTED LINE ///\n                            lineTableTime(time, x);\n                        }\n                    })\n                })\n            })\n\n            if (info.type === \"PROXIMO\") {\n                linesProxi.appendChild(lineNumber);\n            }\n            else {\n                linesFlexo.appendChild(lineNumber);\n            }\n        }\n    }\n})\n\n/// FUNCTION TRANSLATE TIME FORMAT /// UNIVERSAL\nconst changeTimeFormat = (time) => {\n\n    let hours = Math.floor(time / 3600);\n    time %= 3600;\n    let minutes = Math.floor(time / 60);\n    hours = String(hours).padStart(2, \"0\");\n    minutes = String(minutes).padStart(2, \"0\");\n    scheduledArrivalFormatted = `${hours}:${minutes}`; // FORMATED TIME TO SHOW\n}\n\n/// FUNCTION GET : EVERY STATIONS OF SELECTED LINE ///\nfunction lineTableTime(time, x) {\n    for (let o = 0; o < time[x].arrets.length; o++) {\n\n        let lineInfoRow = document.createElement(\"tr\"); // LINE INFO TABLE ROW\n        lineInfoRow.classList.add(\"line-info-row\");\n        lineTime.appendChild(lineInfoRow);\n\n        let lineStation = document.createElement(\"td\"); // LINE INFO TABLE COLUMN\n        lineStation.classList.add(\"line-station\");\n        lineStation.innerText = time[x].arrets[o].parentStation.name;\n        lineInfoRow.appendChild(lineStation);\n\n        /// GET : ALL NEXT TIME STOP FOR EACH STATION ///\n        for (let u = 0; u < 3; u++) {\n\n            if (time[x].arrets[o].trips[u] === \"|\") {\n                let lineTime = document.createElement(\"td\"); // LINE TIME K.O\n                lineTime.classList.add(\"line-time\");\n                lineTime.innerText = \"X\";\n                lineInfoRow.appendChild(lineTime);\n            }\n            else {\n                changeTimeFormat(time[x].arrets[o].trips[u]); // TRANSLATE TIME FUNCTION\n\n                let lineTime = document.createElement(\"td\"); // LINE TIME OK\n                lineTime.classList.add(\"line-time\");\n                lineTime.innerText = scheduledArrivalFormatted;\n                lineInfoRow.appendChild(lineTime);\n            }\n        }\n    }\n}\n\n/// DISPLAY LINES ///\n\nlet tramBtn = document.getElementById(\"tram-btn\");\nlet chronoBtn = document.getElementById(\"chrono-btn\");\nlet proxiBtn = document.getElementById(\"proxi-btn\");\nlet flexoBtn = document.getElementById(\"flexo-btn\");\n\ntramBtn.addEventListener(\"click\", function() {\n\n    if (tramBtn.classList.contains(\"show\")) {\n        linesTram.classList.remove(\"show\");\n        tramBtn.classList.remove(\"show\");\n        linesTram.classList.add(\"hide\");\n        tramBtn.classList.add(\"hide\");\n    }\n    else {\n        linesTram.classList.remove(\"hide\");\n        tramBtn.classList.remove(\"hide\");\n        linesTram.classList.add(\"show\");\n        tramBtn.classList.add(\"show\");\n    }\n})\n\nchronoBtn.addEventListener(\"click\", function() {\n\n    if (chronoBtn.classList.contains(\"show\")) {\n        linesChrono.classList.remove(\"show\");\n        chronoBtn.classList.remove(\"show\");\n        linesChrono.classList.add(\"hide\");\n        chronoBtn.classList.add(\"hide\");\n    }\n    else {\n        linesChrono.classList.remove(\"hide\");\n        chronoBtn.classList.remove(\"hide\");\n        linesChrono.classList.add(\"show\");\n        chronoBtn.classList.add(\"show\");\n    }\n})\n\nproxiBtn.addEventListener(\"click\", function() {\n\n    if (proxiBtn.classList.contains(\"show\")) {\n        linesProxi.classList.remove(\"show\");\n        proxiBtn.classList.remove(\"show\");\n        linesProxi.classList.add(\"hide\");\n        proxiBtn.classList.add(\"hide\");\n    }\n    else {\n        linesProxi.classList.remove(\"hide\");\n        proxiBtn.classList.remove(\"hide\");\n        linesProxi.classList.add(\"show\");\n        proxiBtn.classList.add(\"show\");\n    }\n})\n\nflexoBtn.addEventListener(\"click\", function() {\n\n    if (flexoBtn.classList.contains(\"show\")) {\n        linesFlexo.classList.remove(\"show\");\n        flexoBtn.classList.remove(\"show\");\n        linesFlexo.classList.add(\"hide\");\n        flexoBtn.classList.add(\"hide\");\n    }\n    else {\n        linesFlexo.classList.remove(\"hide\");\n        flexoBtn.classList.remove(\"hide\");\n        linesFlexo.classList.add(\"show\");\n        flexoBtn.classList.add(\"show\");\n    }\n})\n\n//# sourceURL=webpack:///./src/js/horaire.js?");

/***/ })

/******/ });