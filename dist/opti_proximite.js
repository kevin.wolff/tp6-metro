/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/js/proximite.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/js/proximite.js":
/*!*****************************!*\
  !*** ./src/js/proximite.js ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("/// LOCAL STORAGE VARIABLE ///\nlet favObject = {}; // OBJECT STOCK IN LOCAL STORAGE\nfavObject.favoris = []; // ARRAY STOCK IN OBJECT\n\n/// LOCALISATION TEST ///\nlet test = { // ST BRUNO\n  latitude: 45.188331, ///\n  longitude: 5.713552 ///\n};\n\n/// VARIABLE FOR LOADING ANIMATION ///\nlet loader = document.querySelector(\".loader\");\n\n// LOCALISATION SUCCESS\nfunction success(pos) {\n  // let crd = pos.coords; // LOCALISATION COORDINATES\n\n  // navigator.geolocation.clearWatch(id); // STOP LOCALISATION\n\n  /// MAP GENERATOR ///\n  let mymap = L.map('mapidLocalisation').setView([`${test.latitude}`, `${test.longitude}`], 18); // FIRST LAYER\n\n  L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', { // SECOND LAYER\n    maxZoom: 18,\n    id: 'mapbox/streets-v11',\n    tileSize: 512,\n    zoomOffset: -1,\n    accessToken: 'pk.eyJ1Ijoia2V2aW53b2xmZjM4IiwiYSI6ImNrZWExY2VqYzA4ODYyeXQwdzNhMHZhMzgifQ.TR32ViKBTg_E3VduNElxqA'\n  }).addTo(mymap);\n\n  let marker = L.marker([`${test.latitude}`, `${test.longitude}`]).addTo(mymap); // MARKER \"MY POSITION\"\n\n  // LINES MANAGE BY APP\n  linesOK = [\"SEM:A\", \"SEM:B\", \"SEM:C\", \"SEM:D\", \"SEM:E\", \"SEM:C1\", \"SEM:C2\", \"SEM:C3\", \"SEM:C4\", \"SEM:C5\", \"SEM:C6\", \"SEM:C7\", \"SEM:12\", \"SEM:13\", \"SEM:14\", \"SEM:15\", \"SEM:16\", \"SEM:19\", \"SEM:20\", \"SEM:21\", \"SEM:22\", \"SEM:23\", \"SEM:25\", \"SEM:26\", \"SEM:40\", \"SEM:41\", \"SEM:42\", \"SEM:43\", \"SEM:44\", \"SEM:45\", \"SEM:46\", \"SEM:47\", \"SEM:48\", \"SEM:49\", \"SEM:50\", \"SEM:51\", \"SEM:54\", \"SEM:55\", \"SEM:56\", \"SEM:60\", \"SEM:61\", \"SEM:62\", \"SEM:63\", \"SEM:64\", \"SEM:65\", \"SEM:66\", \"SEM:67\", \"SEM:68\", \"SEM:70\", \"SEM:71\"];\n\n  /// REQUEST : NEARBY STOP ///\n  $.ajax({\n    url: `http://data.metromobilite.fr/api/linesNear/json?x=${test.longitude}&y=${test.latitude}&dist=2000&details=false`,\n    dataType: \"json\",\n  }).done(function (data) {\n\n    if (data.length !== 0) {\n\n      /// GET : ALL NEARBY STOP ///\n      for (let i = 0; i < data.length; i++) {\n        let stop = data[i];\n\n        if (linesOK.includes(stop.lines[0])) {\n\n          let marker = L.marker([`${stop.lat}`, `${stop.lon}`]).addTo(mymap); // ADD NEARBY STOP MARKER\n\n          let modal = document.querySelector(\".modal\"); // MODAL DIV - CIBLE HTML\n\n          /// EVENT LISTENER ON STOP MARKER ///\n          marker.addEventListener(\"click\", function () {\n\n            loader.classList.remove(\"hide\");\n            loader.classList.add(\"loading\");\n\n            modal.innerHTML = \"\"; // CLEAN MODAL\n\n            modal.classList.remove(\"hide\"); // REMOVE MODAL \"HIDE\" CLASS\n            modal.classList.add(\"show\"); // ADD MODAL \"SHOW\" CLASS\n\n            let modalHead = document.createElement(\"div\"); // MODAL HEAD\n            modalHead.classList.add(\"modal-head\");\n            modal.appendChild(modalHead);\n\n            let closeModal = document.createElement(\"img\"); // MODAL CLOSE BUTTON\n            closeModal.classList.add(\"closeBtn\");\n            closeModal.setAttribute(\"src\", \"./src/img/close.png\");\n            modalHead.appendChild(closeModal);\n\n            /// EVENT LISTENER ON MODAL CLOSE BUTTON ///\n            closeModal.addEventListener(\"click\", function () {\n              modal.classList.add(\"hide\"); // ADD MODAL \"HIDE\" CLASS\n              modal.classList.remove(\"show\"); // REMOVE MODAL \"SHOW\" CLASS\n            })\n\n            let modalHeadInfo = document.createElement(\"div\"); // MODAL HEAD INFO\n            modalHeadInfo.classList.add(\"modal-head-info\");\n            modalHead.appendChild(modalHeadInfo);\n\n            let favStar = document.createElement(\"img\"); // STOP FAV\n            favStar.classList.add(\"fav-star\");\n\n            // MANAGE HEART FULL/EMPTY FOR FAVORITE\n            if (localStorage.length !==0) {\n              favObject = JSON.parse(localStorage.getItem(\"favLines\")); // GET INIT LOCALSTORAGE\n\n              if (favObject.favoris.includes(stop.id)) {\n                favStar.setAttribute(\"src\", \"./src/img/heartF.png\"); // ALREADY FAV\n                modalHeadInfo.appendChild(favStar);\n              }\n              else {\n                favStar.setAttribute(\"src\", \"./src/img/heart.png\"); // NOT FAV\n                modalHeadInfo.appendChild(favStar);\n              }\n            }\n            else {\n                favStar.setAttribute(\"src\", \"./src/img/heart.png\"); // NOT FAV\n                modalHeadInfo.appendChild(favStar);\n            }\n\n            /// EVENT LISTENER ON FAVORITE ICONE ///\n            favStar.addEventListener(\"click\", function() {\n\n              // IF LOCAL STORAGE EMPTY\n              if (localStorage.length !== 0) {\n\n                favObject = JSON.parse(localStorage.getItem(\"favLines\")); // GET INIT LOCALSTORAGE\n                \n                // IF ID ALREADY STORAGE\n                if (favObject.favoris.includes(stop.id)) {\n                  let index = favObject.favoris.indexOf(stop.id); // GET INDEX OF ID IN THE ARRAY\n                  favObject.favoris.splice(index, 1); // REMOVE ID FROM THE ARRAY\n                  favStar.setAttribute(\"src\", \"./src/img/heart.png\"); // NOT FAV\n                  localStorage.setItem(\"favLines\", JSON.stringify(favObject));\n                }\n\n                // IF ID DON'T STORAGE\n                else {\n                  favObject.favoris.push(stop.id); // ADD LINE ID IN FAVORITE ARRAY\n                  favStar.setAttribute(\"src\", \"./src/img/heartF.png\"); // FAV\n                  localStorage.setItem(\"favLines\", JSON.stringify(favObject));\n                }\n              }\n\n              // IF LOCAL STORAGE NOT EMPTY\n              else {\n                favObject.favoris.push(stop.id); // CREATE LINE ID IN FAVORITE ARRAY\n                favStar.setAttribute(\"src\", \"./src/img/heartF.png\"); // FAV\n                localStorage.setItem(\"favLines\", JSON.stringify(favObject));\n              }\n            })\n\n            let stopSign = document.createElement(\"img\"); // STOP SIGN IMG\n            stopSign.classList.add(\"stop-sign\");\n            stopSign.setAttribute(\"src\", \"./src/img/sign.png\");\n            modalHeadInfo.appendChild(stopSign);\n\n            let stopName = document.createElement(\"p\"); // STOP NAME\n            stopName.classList.add(\"stop-name\");\n\n            let modalBody = document.createElement(\"div\"); // MODAL BODY\n            modalBody.classList.add(\"modal-body\");\n            modal.appendChild(modalBody);\n\n            /// REQUEST : SELECTED LINE INFORMATIONS ///\n            $.ajax({\n              url: `http://data.metromobilite.fr/api/routers/default/index/routes?codes=${stop.lines}`,\n              dataType: \"json\",\n            }).done(function (data) {\n\n              /// GET : ALL LINE INFO ///\n              for (let i = 0; i < data.length; i++) {\n                let info = data[i];\n\n                if (info.type === \"TRAM\" || info.type === \"CHRONO\" || info.type === \"PROXIMO\" || info.type === \"FLEXO\") {\n                  let lineDescr = document.createElement(\"div\"); // LINE DESCRIPTION\n                  lineDescr.classList.add(\"line-descr\");\n                  modalBody.appendChild(lineDescr);\n\n                  let lineNumber = document.createElement(\"div\"); // LINE NUMBER\n                  lineNumber.classList.add(\"line-number\");\n                  lineNumber.style.backgroundColor = `#${info.color}`;\n                  lineNumber.style.boxShadow = \"0px 0px 3px 2px rgba(0,0,0,0.75)\";\n                  lineNumber.innerText = info.shortName;\n                  lineDescr.appendChild(lineNumber);\n\n                  if (info.type === \"CHRONO\") {\n                    lineNumber.style.color = \"#000\";\n                  }\n\n                  let typeMode = document.createElement(\"div\"); // LINE MODE\n                  typeMode.classList.add(\"mode\");\n                  typeMode.style.backgroundColor = `#${info.color}`;\n                  typeMode.style.boxShadow = \"0px 0px 3px 2px rgba(0,0,0,0.75)\";\n                  lineDescr.appendChild(typeMode);\n\n                  if (info.mode == \"BUS\") {\n                    let typeImg = document.createElement(\"img\"); // LINE MODE IMG BUS\n                    typeImg.classList.add(\"type-img\");\n                    typeImg.setAttribute(\"src\", \"./src/img/bus.png\");\n                    typeMode.appendChild(typeImg);\n                  } \n                  else {\n                    let typeImg = document.createElement(\"img\"); // LINE MODE IMG TRAM\n                    typeImg.classList.add(\"type-img\");\n                    typeImg.setAttribute(\"src\", \"./src/img/tram.png\");\n                    typeMode.appendChild(typeImg);\n                  }\n\n                  let lineName = document.createElement(\"p\"); // LINE NAME\n                  lineName.classList.add(\"line-name\");\n                  lineName.innerText = info.longName;\n                  lineDescr.appendChild(lineName);\n                };\n              }\n            });\n\n            /// REQUEST : SELECTED LINE NEXT STOP ///\n            $.ajax({\n              url: `http://data.metromobilite.fr/api/routers/default/index/stops/${stop.id}/stoptimes`,\n              dataType: \"json\",\n            }).done(function (data) {\n\n              if (data.length !== 0) {\n                stopName.innerText = data[0].times[0].stopName; // STOP NAME > MODAL HEAD\n                modalHeadInfo.appendChild(stopName);\n\n                /// GET : ALL LINE NEXT STOP ///\n                for (let i = 0; i < data.length; i++) {\n                  let line = data[i];\n\n                  let lineDest = document.createElement(\"div\"); // LINE DESTINATION AND TIME\n                  lineDest.classList.add(\"line-dest\");\n                  modalBody.appendChild(lineDest);\n\n                  lineDestInfo = document.createElement(\"div\"); // LINE DESTINATION INFO\n                  lineDestInfo.classList.add(\"line-dest-info\");\n                  lineDest.appendChild(lineDestInfo);\n\n                  lineDestImg = document.createElement(\"img\"); // LINE DESTINATION IMG\n                  lineDestImg.classList.add(\"line-dest-img\");\n                  lineDestImg.setAttribute(\"src\", \"./src/img/nextB.png\");\n                  lineDestInfo.appendChild(lineDestImg);\n\n                  let lineLast = document.createElement(\"p\"); // LINE INFO LAST STOP NAME\n                  lineLast.classList.add(\"line-last\");\n                  lineLast.innerHTML = line.pattern.lastStopName;\n                  lineDestInfo.appendChild(lineLast)\n\n                  let lineTime = document.createElement(\"ul\"); // LINE TIME LIST\n                  lineTime.classList.add(\"time\");\n                  lineDest.appendChild(lineTime);\n\n                  if (data[i].times.length > 3) {\n\n                    /// GET : EACH LINE NEXT STOP TIME (DATA > 3) ///\n                    for (let o = 0; o < 3; o++) {\n                      let time = data[i].times[o];\n\n                      changeTimeFormat(time.realtimeArrival); // TRANSLATE TIME FUNCTION\n\n                      time = document.createElement(\"li\"); // LINE NEXT TIME\n                      time.innerText = scheduledArrivalFormatted;\n                      lineTime.appendChild(time);\n\n                      loader.classList.add(\"hide\");\n                      loader.classList.remove(\"loading\");\n                    };\n                  } \n                  else {\n\n                    /// GET : EACH LINE NEXT STOP TIME (DATA < 3) ///\n                    for (let o = 0; o < data[i].times.length; o++) {\n                      let time = data[i].times[o];\n\n                      changeTimeFormat(time.realtimeArrival); // TRANSLATE TIME FUNCTION\n\n                      time = document.createElement(\"li\"); // LINE NEXT TIME\n                      time.innerText = scheduledArrivalFormatted;\n                      lineTime.appendChild(time);\n\n                      loader.classList.add(\"hide\");\n                      loader.classList.remove(\"loading\");\n                    };\n                  };\n                };\n              };\n            });\n          });\n        }\n      };\n    };\n  });\n};\n\n// LOCALISATION ERROR\nfunction error(err) {\n  console.warn('ERROR(' + err.code + '): ' + err.message);\n  console.log(\"erreur dans la localisation\"); // CONSOLE LOG\n};\n\n// LOCALISATION OPTIONS\noptions = {\n  enableHighAccuracy: false,\n  timeout: 5000,\n  maximumAge: 0\n};\n\n// LOCALISATION CALL\n// id = navigator.geolocation.watchPosition(success, error, options);\nsuccess();\n\n/// FUNCTION TRANSLATE TIME FORMAT /// UNIVERSAL\nconst changeTimeFormat = (time) => {\n\n  let hours = Math.floor(time / 3600);\n  time %= 3600;\n  let minutes = Math.floor(time / 60);\n  hours = String(hours).padStart(2, \"0\");\n  minutes = String(minutes).padStart(2, \"0\");\n  scheduledArrivalFormatted = `${hours}:${minutes}`; // FORMATED TIME TO SHOW\n}\n\n//# sourceURL=webpack:///./src/js/proximite.js?");

/***/ })

/******/ });