/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/js/plan.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/js/plan.js":
/*!************************!*\
  !*** ./src/js/plan.js ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("/// MAP GENERATOR ///\n\nlet mymap = L.map('mapidPlan').setView([45.18756, 5.735782], 12); // FIRST LAYER\nL.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', { // SECOND LAYER\n    maxZoom: 18,\n    id: 'mapbox/streets-v11',\n    tileSize: 512,\n    zoomOffset: -1,\n    accessToken: 'pk.eyJ1Ijoia2V2aW53b2xmZjM4IiwiYSI6ImNrZWExY2VqYzA4ODYyeXQwdzNhMHZhMzgifQ.TR32ViKBTg_E3VduNElxqA'\n}).addTo(mymap);\n\n/// HTML LINE TYPE FOR APPENDCHILD ///\n\nlet linesTram = document.querySelector(\".lines-tram\");\nlet linesChrono = document.querySelector(\".lines-chrono\");\nlet linesProxi = document.querySelector(\".lines-proxi\");\nlet linesFlexo = document.querySelector(\".lines-flexo\");\n\n/// REQUETE INFOS ALL LINES ///\n$.ajax({\n    url: `http://data.metromobilite.fr/api/routers/default/index/routes`,\n    dataType: \"json\",\n}).done(function (data) {\n    /// GET : TRAM, CHRONO, PROXIMO & FLEXO LINE INFO ///\n    for (let i = 0; i < data.length; i++) {\n        let info = data[i];\n\n        let linesOnMap = []; // ARRAY LINES DISPLAY\n\n        if (info.type === \"TRAM\" || info.type === \"CHRONO\") {\n\n            let lineNumber = document.createElement(\"div\"); // LINE NUMBER\n            lineNumber.classList.add(\"line-number-circle\");\n            lineNumber.style.backgroundColor = `#${info.color}`;\n            lineNumber.innerText = info.shortName;\n\n            if (info.type === \"CHRONO\") {\n              lineNumber.style.color = \"#000\";\n            }\n\n            /// EVENT LISTENER ON LINE NUMBER ///\n            lineNumber.addEventListener(\"click\", function () {\n\n                if (linesOnMap.includes(info.id)) {\n\n                    // let index = linesOnMap.indexOf(info.id);// GET INDEX OF ID IN THE ARRAY\n                    // linesOnMap.splice(index, 1); // REMOVE ID FROM THE ARRAY\n                }\n                else {\n                    linesOnMap.push(info.id); // PUSH ID IN THE ARRAY\n\n                    /// REQUEST : GEOJSON SELECTED LINE ///\n                    $.ajax({\n                        url: `http://data.metromobilite.fr/api/lines/json?types=ligne&codes=${info.id}`,\n                        dataType: \"json\",\n                    }).done(function (data) {\n\n                        let lineCoordinates = data.features[0].geometry;\n                        let lineColor = data.features[0].properties.COULEUR;\n\n                        L.geoJson(lineCoordinates, {\n                            color: `rgb(${lineColor})`,\n                            weight: 5\n                        }).addTo(mymap);\n\n                        /// REQUEST : INFOS SELECTED LINE ///\n                        $.ajax({\n                            url: `http://data.metromobilite.fr/api/ficheHoraires/json?route=${info.id}`,\n                            dataType: \"json\",\n                        }).done(function (data) {\n\n                            /// GET : ALL STOP LOCALISATION OF SELECTED LINE ///\n                            for (let o = 0; o < data[0].arrets.length; o++) {\n\n                                let test = L.circle([`${data[0].arrets[o].lat}`, `${data[0].arrets[o].lon}`], {\n                                    weight: \"8\",\n                                    color: `rgb(${lineColor})`,\n                                    fillColor: `rgb(${lineColor})`,\n                                    fillOpacity: \"1\"\n                                }).addTo(mymap); // MARKER FOR EACH STOP\n                            }\n                        })\n                    });\n                }\n            })\n\n            if (data[i].type === \"TRAM\") {\n                linesTram.appendChild(lineNumber);\n            }\n            else {\n                linesChrono.appendChild(lineNumber);\n            }\n        }\n\n        else if (info.type === \"PROXIMO\" || info.type === \"FLEXO\") {\n\n            let lineNumber = document.createElement(\"div\"); // LINE NUMBER\n            lineNumber.classList.add(\"line-number-square\");\n            lineNumber.style.backgroundColor = `#${info.color}`;\n            lineNumber.innerText = info.shortName;\n\n            /// EVENT LISTENER ON LINE NUMBER ///\n            lineNumber.addEventListener(\"click\", function () {\n\n                console.log(linesOnMap); // CONSOLE LOG\n\n                if (linesOnMap.includes(info.id)) {\n\n                    // let index = linesOnMap.indexOf(info.id);// GET INDEX OF ID IN THE ARRAY\n                    // linesOnMap.splice(index, 1); // REMOVE ID FROM THE ARRAY\n                }\n                else {\n                    linesOnMap.push(info.id); // PUSH ID IN THE ARRAY\n\n                    /// REQUEST : GEOJSON SELECTED LINE ///\n                    $.ajax({\n                        url: `http://data.metromobilite.fr/api/lines/json?types=ligne&codes=${info.id}`,\n                        dataType: \"json\",\n                    }).done(function (data) {\n\n                        let lineCoordinates = data.features[0].geometry;\n                        let lineColor = data.features[0].properties.COULEUR;\n\n                        L.geoJson(lineCoordinates, {\n                            color: `rgb(${lineColor})`,\n                            weight: 5\n                        }).addTo(mymap);\n\n                        /// REQUEST : INFOS SELECTED LINE ///\n                        $.ajax({\n                            url: `http://data.metromobilite.fr/api/ficheHoraires/json?route=${info.id}`,\n                            dataType: \"json\",\n                        }).done(function (data) {\n    \n                            /// GET : ALL STOP LOCALISATION OF SELECTED LINE ///\n                            for (let o = 0; o < data[0].arrets.length; o++) {\n    \n                                let test = L.circle([`${data[0].arrets[o].lat}`, `${data[0].arrets[o].lon}`], {\n                                    weight: \"8\",\n                                    color: `rgb(${lineColor})`,\n                                    fillColor: `rgb(${lineColor})`,\n                                    fillOpacity: \"1\"\n                                }).addTo(mymap); // MARKER FOR EACH STOP\n                            }\n                        })\n                    });\n                }\n            })\n\n            if (data[i].type === \"PROXIMO\") {\n                linesProxi.appendChild(lineNumber);\n            }\n            else {\n                linesFlexo.appendChild(lineNumber);\n            }\n        }\n    }\n})\n\n/// DISPLAY LINES ///\n\nlet tramBtn = document.getElementById(\"tram-btn\");\nlet chronoBtn = document.getElementById(\"chrono-btn\");\nlet proxiBtn = document.getElementById(\"proxi-btn\");\nlet flexoBtn = document.getElementById(\"flexo-btn\");\n\ntramBtn.addEventListener(\"click\", function() {\n\n    if (tramBtn.classList.contains(\"show\")) {\n        linesTram.classList.remove(\"show\");\n        tramBtn.classList.remove(\"show\");\n        linesTram.classList.add(\"hide\");\n        tramBtn.classList.add(\"hide\");\n    }\n    else {\n        linesTram.classList.remove(\"hide\");\n        tramBtn.classList.remove(\"hide\");\n        linesTram.classList.add(\"show\");\n        tramBtn.classList.add(\"show\");\n    }\n})\n\nchronoBtn.addEventListener(\"click\", function() {\n\n    if (chronoBtn.classList.contains(\"show\")) {\n        linesChrono.classList.remove(\"show\");\n        chronoBtn.classList.remove(\"show\");\n        linesChrono.classList.add(\"hide\");\n        chronoBtn.classList.add(\"hide\");\n    }\n    else {\n        linesChrono.classList.remove(\"hide\");\n        chronoBtn.classList.remove(\"hide\");\n        linesChrono.classList.add(\"show\");\n        chronoBtn.classList.add(\"show\");\n    }\n})\n\nproxiBtn.addEventListener(\"click\", function() {\n\n    if (proxiBtn.classList.contains(\"show\")) {\n        linesProxi.classList.remove(\"show\");\n        proxiBtn.classList.remove(\"show\");\n        linesProxi.classList.add(\"hide\");\n        proxiBtn.classList.add(\"hide\");\n    }\n    else {\n        linesProxi.classList.remove(\"hide\");\n        proxiBtn.classList.remove(\"hide\");\n        linesProxi.classList.add(\"show\");\n        proxiBtn.classList.add(\"show\");\n    }\n})\n\nflexoBtn.addEventListener(\"click\", function() {\n\n    if (flexoBtn.classList.contains(\"show\")) {\n        linesFlexo.classList.remove(\"show\");\n        flexoBtn.classList.remove(\"show\");\n        linesFlexo.classList.add(\"hide\");\n        flexoBtn.classList.add(\"hide\");\n    }\n    else {\n        linesFlexo.classList.remove(\"hide\");\n        flexoBtn.classList.remove(\"hide\");\n        linesFlexo.classList.add(\"show\");\n        flexoBtn.classList.add(\"show\");\n    }\n})\n\n//# sourceURL=webpack:///./src/js/plan.js?");

/***/ })

/******/ });