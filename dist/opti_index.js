/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/js/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/js/index.js":
/*!*************************!*\
  !*** ./src/js/index.js ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("/// LOCAL STORAGE VARIABLE ///\nlet favObject = {}; // OBJECT STOCK IN LOCAL STORAGE\nfavObject.favoris = []; // ARRAY STOCK IN OBJECT\n\n/// VARIABLE FOR LOADING ANIMATION ///\nlet loader = document.querySelector(\".loader\");\n\n/// COVID BANNER ///\nlet covidAlert = document.querySelector(\".alert\"); // ALERT DIV\nlet alertClose = document.getElementById(\"alert-close\"); // ALERT CLOSE BTN\n\n// FUNCTION : MAKE ALERT UP\nfunction alertPop() {\n  covidAlert.classList.remove(\"hide\");\n  covidAlert.classList.add(\"show\");\n}\n\n// INTERVAL EVERY 3MN LUNCH MAKE ALERT UP\nlet interval = setInterval(function(){ alertPop() }, 10000);\n\nalertClose.addEventListener(\"click\", function() {\n  covidAlert.classList.remove(\"show\");\n  covidAlert.classList.add(\"hide\");\n})\n\n/// FAVORITES STOP ///\n\nlet favorites = document.querySelector(\".favorites\"); // HTML TARGET\n\nlet favLines = JSON.parse(localStorage.getItem(\"favLines\")); // GET LOCAL STORAGE\n\nif (favLines !== null && favLines.favoris.length !== 0) {\n  /// GET : ALL FAVORITES STOP FROM LOCAL STORAGE ///\n  for (let i = 0; i < favLines.favoris.length; i ++) {\n    let favLine = favLines.favoris[i];\n\n    loader.classList.remove(\"hide\");\n    loader.classList.add(\"loading\");\n    \n    /// REQUEST : SELECTED LINE INFORMATIONS ///\n    $.ajax({\n    url: `http://data.metromobilite.fr/api/routers/default/index/stops/${favLine}/stoptimes`,\n    dataType: \"json\",\n    }).done(function (data) {\n        \n      /// GET : ALL INFORMATIONS ABOUT SELECTED LINE ///\n      for (let o = 0; o < data.length; o++) {\n        let info = data[o];\n\n        let stopInfo = document.createElement(\"div\"); // STOP INFORMATIONS DIV\n        stopInfo.classList.add(\"stop-info\");\n        favorites.appendChild(stopInfo);\n\n        let stopName = document.createElement(\"div\"); // STOP NAME DIV\n        stopName.classList.add(\"stop-name\");\n        stopInfo.appendChild(stopName);\n\n        let favStar = document.createElement(\"img\"); // FAV IMG\n        favStar.classList.add(\"fav-star\");\n\n        // MANAGE HEART FULL/EMPTY FOR FAVORITE\n        if (localStorage.length !== 0) {\n          favObject = JSON.parse(localStorage.getItem(\"favLines\")); // GET INIT LOCALSTORAGE\n\n          if (favObject.favoris.includes(info.times[0].stopId)) {\n            favStar.setAttribute(\"src\", \"./src/img/heartBF.png\"); // ALREADY FAV\n            stopName.appendChild(favStar);\n          }\n          else {\n            favStar.setAttribute(\"src\", \"./src/img/heartB.png\"); // NOT FAV\n            stopName.appendChild(favStar);\n          }\n        }\n\n        /// EVENT LISTENER ON FAVORITE ICON ///\n        favStar.addEventListener(\"click\", function() {\n\n          favObject = JSON.parse(localStorage.getItem(\"favLines\")); // GET INIT LOCALSTORAGE\n            \n          // IF ID ALREADY STORAGE\n          if (favObject.favoris.includes(info.times[0].stopId)) {\n            let index = favObject.favoris.indexOf(info.times[0].stopId); // GET INDEX OF ID IN THE ARRAY\n            favObject.favoris.splice(index, 1); // REMOVE ID FROM THE ARRAY\n            favStar.setAttribute(\"src\", \"./src/img/heartB.png\"); // NOT FAV\n            localStorage.setItem(\"favLines\", JSON.stringify(favObject));\n          }\n\n          // IF ID DON'T STORAGE\n          else {\n            favObject.favoris.push(info.times[0].stopId); // ADD LINE ID IN FAVORITE ARRAY\n            favStar.setAttribute(\"src\", \"./src/img/heartBF.png\"); // FAV\n            localStorage.setItem(\"favLines\", JSON.stringify(favObject));\n          }\n        })\n\n        let stopImg = document.createElement(\"img\"); // SIGN IMAGE\n        stopImg.setAttribute(\"src\", \"./src/img/signB.png\");\n        stopName.appendChild(stopImg);\n\n        let lineName = document.createElement(\"p\"); // STOP NAME\n        lineName.classList.add(\"line-name\");\n        stopName.appendChild(lineName);\n\n        let stopDirec = document.createElement(\"div\"); // STOP DIRECTION DIV\n        stopDirec.classList.add(\"stop-direc\");\n        stopInfo.appendChild(stopDirec);\n\n        let direcImg = document.createElement(\"img\"); // DIRECTION IMG\n        direcImg.setAttribute(\"src\", \"./src/img/nextB.png\");\n        stopDirec.appendChild(direcImg);\n\n        let lineDirec = document.createElement(\"p\"); // STOP DIRECTION\n        lineDirec.classList.add(\"line-direc\");\n        lineDirec.innerText = info.pattern.lastStopName;\n        stopDirec.appendChild(lineDirec);\n\n        let stopTimes = document.createElement(\"ul\"); // ALL STOP TIMES\n        stopTimes.classList.add(\"stop-times\");\n        stopInfo.appendChild(stopTimes);\n\n        /// GET : EACH LINE TWO NEXT STOP TIME ///\n        for (let o = 0; o < 2; o++) {\n          let time = info.times[o];\n\n          lineName.innerText = time.stopName; // STOP NAME > STOP INFORMATIONS DIV\n\n          changeTimeFormat(time.realtimeArrival); // TRANSLATE TIME FUNCTION\n\n          let stopTime = document.createElement(\"li\"); // STOP TIME\n          stopTime.innerText = scheduledArrivalFormatted;\n          stopTimes.appendChild(stopTime);\n\n          loader.classList.add(\"hide\");\n          loader.classList.remove(\"loading\");\n        };\n      }\n    })\n  }\n}\nelse {\n  let noFav = document.createElement(\"p\");\n  noFav.innerText = \"Vous n'avez pas d'arrêt(s) favori(s)\";\n  favorites.appendChild(noFav);\n}\n\n/// FUNCTION TRANSLATE TIME FORMAT /// UNIVERSAL\nconst changeTimeFormat = (time) => {\n\n  let hours = Math.floor(time / 3600);\n  time %= 3600;\n  let minutes = Math.floor(time / 60);\n  hours = String(hours).padStart(2, \"0\");\n  minutes = String(minutes).padStart(2, \"0\");\n  scheduledArrivalFormatted = `${hours}:${minutes}`; // FORMATED TIME TO SHOW\n}\n\n//# sourceURL=webpack:///./src/js/index.js?");

/***/ })

/******/ });