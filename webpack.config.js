const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
    mode: 'development',
  entry: {
      index: './src/js/index.js',
      horaire: './src/js/horaire.js',
      plan: './src/js/plan.js',
      proximite: './src/js/proximite.js'
  },
  output: {
    filename: 'opti_[name].js',
    path: path.resolve(__dirname, 'dist'), // ./dist/
  },
  plugins: [
    new MiniCssExtractPlugin()
    ],
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader',
        ],
      },
      {
        test: /\.s[ac]ss$/,
        use: [
            MiniCssExtractPlugin.loader,
            'css-loader',
            'sass-loader'
        ]
      }
    ],
  },
};