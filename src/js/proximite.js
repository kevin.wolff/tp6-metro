/// LOCAL STORAGE VARIABLE ///
let favObject = {}; // OBJECT STOCK IN LOCAL STORAGE
favObject.favoris = []; // ARRAY STOCK IN OBJECT

/// LOCALISATION TEST ///
let test = { // ST BRUNO
  latitude: 45.188331, ///
  longitude: 5.713552 ///
};

/// VARIABLE FOR LOADING ANIMATION ///
let loader = document.querySelector(".loader");

// LOCALISATION SUCCESS
function success(pos) {
  // let crd = pos.coords; // LOCALISATION COORDINATES

  // navigator.geolocation.clearWatch(id); // STOP LOCALISATION

  /// MAP GENERATOR ///
  let mymap = L.map('mapidLocalisation').setView([`${test.latitude}`, `${test.longitude}`], 18); // FIRST LAYER

  L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', { // SECOND LAYER
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1Ijoia2V2aW53b2xmZjM4IiwiYSI6ImNrZWExY2VqYzA4ODYyeXQwdzNhMHZhMzgifQ.TR32ViKBTg_E3VduNElxqA'
  }).addTo(mymap);

  let marker = L.marker([`${test.latitude}`, `${test.longitude}`]).addTo(mymap); // MARKER "MY POSITION"

  // LINES MANAGE BY APP
  linesOK = ["SEM:A", "SEM:B", "SEM:C", "SEM:D", "SEM:E", "SEM:C1", "SEM:C2", "SEM:C3", "SEM:C4", "SEM:C5", "SEM:C6", "SEM:C7", "SEM:12", "SEM:13", "SEM:14", "SEM:15", "SEM:16", "SEM:19", "SEM:20", "SEM:21", "SEM:22", "SEM:23", "SEM:25", "SEM:26", "SEM:40", "SEM:41", "SEM:42", "SEM:43", "SEM:44", "SEM:45", "SEM:46", "SEM:47", "SEM:48", "SEM:49", "SEM:50", "SEM:51", "SEM:54", "SEM:55", "SEM:56", "SEM:60", "SEM:61", "SEM:62", "SEM:63", "SEM:64", "SEM:65", "SEM:66", "SEM:67", "SEM:68", "SEM:70", "SEM:71"];

  /// REQUEST : NEARBY STOP ///
  $.ajax({
    url: `http://data.metromobilite.fr/api/linesNear/json?x=${test.longitude}&y=${test.latitude}&dist=2000&details=false`,
    dataType: "json",
  }).done(function (data) {

    if (data.length !== 0) {

      /// GET : ALL NEARBY STOP ///
      for (let i = 0; i < data.length; i++) {
        let stop = data[i];

        if (linesOK.includes(stop.lines[0])) {

          let marker = L.marker([`${stop.lat}`, `${stop.lon}`]).addTo(mymap); // ADD NEARBY STOP MARKER

          let modal = document.querySelector(".modal"); // MODAL DIV - CIBLE HTML

          /// EVENT LISTENER ON STOP MARKER ///
          marker.addEventListener("click", function () {

            loader.classList.remove("hide");
            loader.classList.add("loading");

            modal.innerHTML = ""; // CLEAN MODAL

            modal.classList.remove("hide"); // REMOVE MODAL "HIDE" CLASS
            modal.classList.add("show"); // ADD MODAL "SHOW" CLASS

            let modalHead = document.createElement("div"); // MODAL HEAD
            modalHead.classList.add("modal-head");
            modal.appendChild(modalHead);

            let closeModal = document.createElement("img"); // MODAL CLOSE BUTTON
            closeModal.classList.add("closeBtn");
            closeModal.setAttribute("src", "./src/img/close.png");
            modalHead.appendChild(closeModal);

            /// EVENT LISTENER ON MODAL CLOSE BUTTON ///
            closeModal.addEventListener("click", function () {
              modal.classList.add("hide"); // ADD MODAL "HIDE" CLASS
              modal.classList.remove("show"); // REMOVE MODAL "SHOW" CLASS
            })

            let modalHeadInfo = document.createElement("div"); // MODAL HEAD INFO
            modalHeadInfo.classList.add("modal-head-info");
            modalHead.appendChild(modalHeadInfo);

            let favStar = document.createElement("img"); // STOP FAV
            favStar.classList.add("fav-star");

            // MANAGE HEART FULL/EMPTY FOR FAVORITE
            if (localStorage.length !==0) {
              favObject = JSON.parse(localStorage.getItem("favLines")); // GET INIT LOCALSTORAGE

              if (favObject.favoris.includes(stop.id)) {
                favStar.setAttribute("src", "./src/img/heartF.png"); // ALREADY FAV
                modalHeadInfo.appendChild(favStar);
              }
              else {
                favStar.setAttribute("src", "./src/img/heart.png"); // NOT FAV
                modalHeadInfo.appendChild(favStar);
              }
            }
            else {
                favStar.setAttribute("src", "./src/img/heart.png"); // NOT FAV
                modalHeadInfo.appendChild(favStar);
            }

            /// EVENT LISTENER ON FAVORITE ICONE ///
            favStar.addEventListener("click", function() {

              // IF LOCAL STORAGE EMPTY
              if (localStorage.length !== 0) {

                favObject = JSON.parse(localStorage.getItem("favLines")); // GET INIT LOCALSTORAGE
                
                // IF ID ALREADY STORAGE
                if (favObject.favoris.includes(stop.id)) {
                  let index = favObject.favoris.indexOf(stop.id); // GET INDEX OF ID IN THE ARRAY
                  favObject.favoris.splice(index, 1); // REMOVE ID FROM THE ARRAY
                  favStar.setAttribute("src", "./src/img/heart.png"); // NOT FAV
                  localStorage.setItem("favLines", JSON.stringify(favObject));
                }

                // IF ID DON'T STORAGE
                else {
                  favObject.favoris.push(stop.id); // ADD LINE ID IN FAVORITE ARRAY
                  favStar.setAttribute("src", "./src/img/heartF.png"); // FAV
                  localStorage.setItem("favLines", JSON.stringify(favObject));
                }
              }

              // IF LOCAL STORAGE NOT EMPTY
              else {
                favObject.favoris.push(stop.id); // CREATE LINE ID IN FAVORITE ARRAY
                favStar.setAttribute("src", "./src/img/heartF.png"); // FAV
                localStorage.setItem("favLines", JSON.stringify(favObject));
              }
            })

            let stopSign = document.createElement("img"); // STOP SIGN IMG
            stopSign.classList.add("stop-sign");
            stopSign.setAttribute("src", "./src/img/sign.png");
            modalHeadInfo.appendChild(stopSign);

            let stopName = document.createElement("p"); // STOP NAME
            stopName.classList.add("stop-name");

            let modalBody = document.createElement("div"); // MODAL BODY
            modalBody.classList.add("modal-body");
            modal.appendChild(modalBody);

            /// REQUEST : SELECTED LINE INFORMATIONS ///
            $.ajax({
              url: `http://data.metromobilite.fr/api/routers/default/index/routes?codes=${stop.lines}`,
              dataType: "json",
            }).done(function (data) {

              /// GET : ALL LINE INFO ///
              for (let i = 0; i < data.length; i++) {
                let info = data[i];

                if (info.type === "TRAM" || info.type === "CHRONO" || info.type === "PROXIMO" || info.type === "FLEXO") {
                  let lineDescr = document.createElement("div"); // LINE DESCRIPTION
                  lineDescr.classList.add("line-descr");
                  modalBody.appendChild(lineDescr);

                  let lineNumber = document.createElement("div"); // LINE NUMBER
                  lineNumber.classList.add("line-number");
                  lineNumber.style.backgroundColor = `#${info.color}`;
                  lineNumber.style.boxShadow = "0px 0px 3px 2px rgba(0,0,0,0.75)";
                  lineNumber.innerText = info.shortName;
                  lineDescr.appendChild(lineNumber);

                  if (info.type === "CHRONO") {
                    lineNumber.style.color = "#000";
                  }

                  let typeMode = document.createElement("div"); // LINE MODE
                  typeMode.classList.add("mode");
                  typeMode.style.backgroundColor = `#${info.color}`;
                  typeMode.style.boxShadow = "0px 0px 3px 2px rgba(0,0,0,0.75)";
                  lineDescr.appendChild(typeMode);

                  if (info.mode == "BUS") {
                    let typeImg = document.createElement("img"); // LINE MODE IMG BUS
                    typeImg.classList.add("type-img");
                    typeImg.setAttribute("src", "./src/img/bus.png");
                    typeMode.appendChild(typeImg);
                  } 
                  else {
                    let typeImg = document.createElement("img"); // LINE MODE IMG TRAM
                    typeImg.classList.add("type-img");
                    typeImg.setAttribute("src", "./src/img/tram.png");
                    typeMode.appendChild(typeImg);
                  }

                  let lineName = document.createElement("p"); // LINE NAME
                  lineName.classList.add("line-name");
                  lineName.innerText = info.longName;
                  lineDescr.appendChild(lineName);
                };
              }
            });

            /// REQUEST : SELECTED LINE NEXT STOP ///
            $.ajax({
              url: `http://data.metromobilite.fr/api/routers/default/index/stops/${stop.id}/stoptimes`,
              dataType: "json",
            }).done(function (data) {

              if (data.length !== 0) {
                stopName.innerText = data[0].times[0].stopName; // STOP NAME > MODAL HEAD
                modalHeadInfo.appendChild(stopName);

                /// GET : ALL LINE NEXT STOP ///
                for (let i = 0; i < data.length; i++) {
                  let line = data[i];

                  let lineDest = document.createElement("div"); // LINE DESTINATION AND TIME
                  lineDest.classList.add("line-dest");
                  modalBody.appendChild(lineDest);

                  lineDestInfo = document.createElement("div"); // LINE DESTINATION INFO
                  lineDestInfo.classList.add("line-dest-info");
                  lineDest.appendChild(lineDestInfo);

                  lineDestImg = document.createElement("img"); // LINE DESTINATION IMG
                  lineDestImg.classList.add("line-dest-img");
                  lineDestImg.setAttribute("src", "./src/img/nextB.png");
                  lineDestInfo.appendChild(lineDestImg);

                  let lineLast = document.createElement("p"); // LINE INFO LAST STOP NAME
                  lineLast.classList.add("line-last");
                  lineLast.innerHTML = line.pattern.lastStopName;
                  lineDestInfo.appendChild(lineLast)

                  let lineTime = document.createElement("ul"); // LINE TIME LIST
                  lineTime.classList.add("time");
                  lineDest.appendChild(lineTime);

                  if (data[i].times.length > 3) {

                    /// GET : EACH LINE NEXT STOP TIME (DATA > 3) ///
                    for (let o = 0; o < 3; o++) {
                      let time = data[i].times[o];

                      changeTimeFormat(time.realtimeArrival); // TRANSLATE TIME FUNCTION

                      time = document.createElement("li"); // LINE NEXT TIME
                      time.innerText = scheduledArrivalFormatted;
                      lineTime.appendChild(time);

                      loader.classList.add("hide");
                      loader.classList.remove("loading");
                    };
                  } 
                  else {

                    /// GET : EACH LINE NEXT STOP TIME (DATA < 3) ///
                    for (let o = 0; o < data[i].times.length; o++) {
                      let time = data[i].times[o];

                      changeTimeFormat(time.realtimeArrival); // TRANSLATE TIME FUNCTION

                      time = document.createElement("li"); // LINE NEXT TIME
                      time.innerText = scheduledArrivalFormatted;
                      lineTime.appendChild(time);

                      loader.classList.add("hide");
                      loader.classList.remove("loading");
                    };
                  };
                };
              };
            });
          });
        }
      };
    };
  });
};

// LOCALISATION ERROR
function error(err) {
  console.warn('ERROR(' + err.code + '): ' + err.message);
  console.log("erreur dans la localisation"); // CONSOLE LOG
};

// LOCALISATION OPTIONS
options = {
  enableHighAccuracy: false,
  timeout: 5000,
  maximumAge: 0
};

// LOCALISATION CALL
// id = navigator.geolocation.watchPosition(success, error, options);
success();

/// FUNCTION TRANSLATE TIME FORMAT /// UNIVERSAL
const changeTimeFormat = (time) => {

  let hours = Math.floor(time / 3600);
  time %= 3600;
  let minutes = Math.floor(time / 60);
  hours = String(hours).padStart(2, "0");
  minutes = String(minutes).padStart(2, "0");
  scheduledArrivalFormatted = `${hours}:${minutes}`; // FORMATED TIME TO SHOW
}