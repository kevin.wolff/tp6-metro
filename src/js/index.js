/// LOCAL STORAGE VARIABLE ///
let favObject = {}; // OBJECT STOCK IN LOCAL STORAGE
favObject.favoris = []; // ARRAY STOCK IN OBJECT

/// VARIABLE FOR LOADING ANIMATION ///
let loader = document.querySelector(".loader");

/// COVID BANNER ///
let covidAlert = document.querySelector(".alert"); // ALERT DIV
let alertClose = document.getElementById("alert-close"); // ALERT CLOSE BTN

// FUNCTION : MAKE ALERT UP
function alertPop() {
  covidAlert.classList.remove("hide");
  covidAlert.classList.add("show");
}

// INTERVAL EVERY 3MN LUNCH MAKE ALERT UP
let interval = setInterval(function(){ alertPop() }, 10000);

alertClose.addEventListener("click", function() {
  covidAlert.classList.remove("show");
  covidAlert.classList.add("hide");
})

/// FAVORITES STOP ///

let favorites = document.querySelector(".favorites"); // HTML TARGET

let favLines = JSON.parse(localStorage.getItem("favLines")); // GET LOCAL STORAGE

if (favLines !== null && favLines.favoris.length !== 0) {
  /// GET : ALL FAVORITES STOP FROM LOCAL STORAGE ///
  for (let i = 0; i < favLines.favoris.length; i ++) {
    let favLine = favLines.favoris[i];

    loader.classList.remove("hide");
    loader.classList.add("loading");
    
    /// REQUEST : SELECTED LINE INFORMATIONS ///
    $.ajax({
    url: `http://data.metromobilite.fr/api/routers/default/index/stops/${favLine}/stoptimes`,
    dataType: "json",
    }).done(function (data) {
        
      /// GET : ALL INFORMATIONS ABOUT SELECTED LINE ///
      for (let o = 0; o < data.length; o++) {
        let info = data[o];

        let stopInfo = document.createElement("div"); // STOP INFORMATIONS DIV
        stopInfo.classList.add("stop-info");
        favorites.appendChild(stopInfo);

        let stopName = document.createElement("div"); // STOP NAME DIV
        stopName.classList.add("stop-name");
        stopInfo.appendChild(stopName);

        let favStar = document.createElement("img"); // FAV IMG
        favStar.classList.add("fav-star");

        // MANAGE HEART FULL/EMPTY FOR FAVORITE
        if (localStorage.length !== 0) {
          favObject = JSON.parse(localStorage.getItem("favLines")); // GET INIT LOCALSTORAGE

          if (favObject.favoris.includes(info.times[0].stopId)) {
            favStar.setAttribute("src", "./src/img/heartBF.png"); // ALREADY FAV
            stopName.appendChild(favStar);
          }
          else {
            favStar.setAttribute("src", "./src/img/heartB.png"); // NOT FAV
            stopName.appendChild(favStar);
          }
        }

        /// EVENT LISTENER ON FAVORITE ICON ///
        favStar.addEventListener("click", function() {

          favObject = JSON.parse(localStorage.getItem("favLines")); // GET INIT LOCALSTORAGE
            
          // IF ID ALREADY STORAGE
          if (favObject.favoris.includes(info.times[0].stopId)) {
            let index = favObject.favoris.indexOf(info.times[0].stopId); // GET INDEX OF ID IN THE ARRAY
            favObject.favoris.splice(index, 1); // REMOVE ID FROM THE ARRAY
            favStar.setAttribute("src", "./src/img/heartB.png"); // NOT FAV
            localStorage.setItem("favLines", JSON.stringify(favObject));
          }

          // IF ID DON'T STORAGE
          else {
            favObject.favoris.push(info.times[0].stopId); // ADD LINE ID IN FAVORITE ARRAY
            favStar.setAttribute("src", "./src/img/heartBF.png"); // FAV
            localStorage.setItem("favLines", JSON.stringify(favObject));
          }
        })

        let stopImg = document.createElement("img"); // SIGN IMAGE
        stopImg.setAttribute("src", "./src/img/signB.png");
        stopName.appendChild(stopImg);

        let lineName = document.createElement("p"); // STOP NAME
        lineName.classList.add("line-name");
        stopName.appendChild(lineName);

        let stopDirec = document.createElement("div"); // STOP DIRECTION DIV
        stopDirec.classList.add("stop-direc");
        stopInfo.appendChild(stopDirec);

        let direcImg = document.createElement("img"); // DIRECTION IMG
        direcImg.setAttribute("src", "./src/img/nextB.png");
        stopDirec.appendChild(direcImg);

        let lineDirec = document.createElement("p"); // STOP DIRECTION
        lineDirec.classList.add("line-direc");
        lineDirec.innerText = info.pattern.lastStopName;
        stopDirec.appendChild(lineDirec);

        let stopTimes = document.createElement("ul"); // ALL STOP TIMES
        stopTimes.classList.add("stop-times");
        stopInfo.appendChild(stopTimes);

        /// GET : EACH LINE TWO NEXT STOP TIME ///
        for (let o = 0; o < 2; o++) {
          let time = info.times[o];

          lineName.innerText = time.stopName; // STOP NAME > STOP INFORMATIONS DIV

          changeTimeFormat(time.realtimeArrival); // TRANSLATE TIME FUNCTION

          let stopTime = document.createElement("li"); // STOP TIME
          stopTime.innerText = scheduledArrivalFormatted;
          stopTimes.appendChild(stopTime);

          loader.classList.add("hide");
          loader.classList.remove("loading");
        };
      }
    })
  }
}
else {
  let noFav = document.createElement("p");
  noFav.innerText = "Vous n'avez pas d'arrêt(s) favori(s)";
  favorites.appendChild(noFav);
}

/// FUNCTION TRANSLATE TIME FORMAT /// UNIVERSAL
const changeTimeFormat = (time) => {

  let hours = Math.floor(time / 3600);
  time %= 3600;
  let minutes = Math.floor(time / 60);
  hours = String(hours).padStart(2, "0");
  minutes = String(minutes).padStart(2, "0");
  scheduledArrivalFormatted = `${hours}:${minutes}`; // FORMATED TIME TO SHOW
}