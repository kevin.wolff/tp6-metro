/// HTML TARGET FOR APPENDCHILD ///

let linesTram = document.querySelector(".lines-tram");
let linesChrono = document.querySelector(".lines-chrono");
let linesProxi = document.querySelector(".lines-proxi");
let linesFlexo = document.querySelector(".lines-flexo");
let lineInfo = document.querySelector(".line-info");
let lineControls = document.querySelector(".line-controls");
let reverseDirect = document.getElementById("reverse");
let lineTime = document.querySelector(".line-time");

/// REQUETE INFOS ALL LINES ///
$.ajax({
    url: `http://data.metromobilite.fr/api/routers/default/index/routes`,
    dataType: "json",
}).done(function (data) {
    
    /// GET : TRAM, CHRONO, PROXIMO & FLEXO LINE INFO ///
    for (let i = 0; i < data.length; i++) {
        let info = data[i];

        if (info.type === "TRAM" || info.type === "CHRONO") {

            let lineNumber = document.createElement("div"); // LINE NUMBER
            lineNumber.classList.add("line-number-circle");
            lineNumber.style.backgroundColor = `#${info.color}`;
            lineNumber.innerText = info.shortName;

            if (info.type === "CHRONO") {
                lineNumber.style.color = "#000";
            }

            /// EVENT LISTENER ON LINE NUMBER ///
            lineNumber.addEventListener("click", function () {

                lineInfo.innerHTML = ""; // EMPTY LINE INFO
                lineTime.innerHTML = ""; // EMPTY LINE TIME

                let lineName = document.createElement("div"); // LINE NAME
                lineName.classList.add("line-name");
                lineName.style.backgroundColor = `#${info.color}`;
                lineName.innerText = info.shortName;
                lineInfo.appendChild(lineName);

                let lineDirect = document.createElement("p"); // LINE DIRECTION
                lineDirect.classList.add("line-direct")
                lineDirect.innerText = info.longName;
                lineInfo.appendChild(lineDirect);

                lineControls.classList.remove("hide");
                lineControls.classList.add("show");

                /// REQUEST : INFOS SELECTED LINE ///
                $.ajax({
                    url: `http://data.metromobilite.fr/api/ficheHoraires/json?route=${info.id}`,
                    dataType: "json",
                }).done(function (data) {
                    let time = data;

                    let x = 0; // VALUE FOR CHOOSE LINE DIRECTION

                    /// GET : EVERY STATIONS OF SELECTED LINE ///
                    lineTableTime(time, x);

                    reverseDirect.addEventListener("click", function () {

                        lineTime.innerHTML = ""; // EMPTY LINE TIME

                        if (x === 0) {
                            x = 1;

                            /// GET : EVERY STATIONS OF SELECTED LINE ///
                            lineTableTime(time, x);
                        }
                        else {
                            x = 0;

                            /// GET : EVERY STATIONS OF SELECTED LINE ///
                            lineTableTime(time, x);
                        }
                    })
                })
            })

            if (info.type === "TRAM") {
                linesTram.appendChild(lineNumber);
            }
            else {
                linesChrono.appendChild(lineNumber);
            }
        }

        else if (info.type === "PROXIMO" || info.type === "FLEXO") {

            let lineNumber = document.createElement("div"); // LINE NUMBER
            lineNumber.classList.add("line-number-square");
            lineNumber.style.backgroundColor = `#${info.color}`;
            lineNumber.innerText = info.shortName;

            /// EVENT LISTENER ON LINE NUMBER ///
            lineNumber.addEventListener("click", function () {

                lineInfo.innerHTML = ""; // EMPTY LINE INFO
                lineTime.innerHTML = ""; // EMPTY LINE TIME

                let lineName = document.createElement("div"); // LINE NAME
                lineName.classList.add("line-name");
                lineName.style.backgroundColor = `#${info.color}`;
                lineName.innerText = info.shortName;
                lineInfo.appendChild(lineName);

                let lineDirect = document.createElement("p"); // LINE DIRECTION
                lineDirect.classList.add("line-direct")
                lineDirect.innerText = info.longName;
                lineInfo.appendChild(lineDirect);

                /// REQUEST : INFOS SELECTED LINE ///
                $.ajax({
                    url: `http://data.metromobilite.fr/api/ficheHoraires/json?route=${info.id}`,
                    dataType: "json",
                }).done(function (data) {
                    let time = data;

                    let x = 0; // VALUE FOR CHOOSE LINE DIRECTION

                    /// GET : EVERY STATIONS OF SELECTED LINE ///
                    lineTableTime(time, x);

                    reverseDirect.addEventListener("click", function () {

                        lineTime.innerHTML = ""; // EMPTY LINE TIME

                        if (x === 0) {
                            x = 1;

                            /// GET : EVERY STATIONS OF SELECTED LINE ///
                            lineTableTime(time, x);
                        }
                        else {
                            x = 0;

                            /// GET : EVERY STATIONS OF SELECTED LINE ///
                            lineTableTime(time, x);
                        }
                    })
                })
            })

            if (info.type === "PROXIMO") {
                linesProxi.appendChild(lineNumber);
            }
            else {
                linesFlexo.appendChild(lineNumber);
            }
        }
    }
})

/// FUNCTION TRANSLATE TIME FORMAT /// UNIVERSAL
const changeTimeFormat = (time) => {

    let hours = Math.floor(time / 3600);
    time %= 3600;
    let minutes = Math.floor(time / 60);
    hours = String(hours).padStart(2, "0");
    minutes = String(minutes).padStart(2, "0");
    scheduledArrivalFormatted = `${hours}:${minutes}`; // FORMATED TIME TO SHOW
}

/// FUNCTION GET : EVERY STATIONS OF SELECTED LINE ///
function lineTableTime(time, x) {
    for (let o = 0; o < time[x].arrets.length; o++) {

        let lineInfoRow = document.createElement("tr"); // LINE INFO TABLE ROW
        lineInfoRow.classList.add("line-info-row");
        lineTime.appendChild(lineInfoRow);

        let lineStation = document.createElement("td"); // LINE INFO TABLE COLUMN
        lineStation.classList.add("line-station");
        lineStation.innerText = time[x].arrets[o].parentStation.name;
        lineInfoRow.appendChild(lineStation);

        /// GET : ALL NEXT TIME STOP FOR EACH STATION ///
        for (let u = 0; u < 3; u++) {

            if (time[x].arrets[o].trips[u] === "|") {
                let lineTime = document.createElement("td"); // LINE TIME K.O
                lineTime.classList.add("line-time");
                lineTime.innerText = "X";
                lineInfoRow.appendChild(lineTime);
            }
            else {
                changeTimeFormat(time[x].arrets[o].trips[u]); // TRANSLATE TIME FUNCTION

                let lineTime = document.createElement("td"); // LINE TIME OK
                lineTime.classList.add("line-time");
                lineTime.innerText = scheduledArrivalFormatted;
                lineInfoRow.appendChild(lineTime);
            }
        }
    }
}

/// DISPLAY LINES ///

let tramBtn = document.getElementById("tram-btn");
let chronoBtn = document.getElementById("chrono-btn");
let proxiBtn = document.getElementById("proxi-btn");
let flexoBtn = document.getElementById("flexo-btn");

tramBtn.addEventListener("click", function() {

    if (tramBtn.classList.contains("show")) {
        linesTram.classList.remove("show");
        tramBtn.classList.remove("show");
        linesTram.classList.add("hide");
        tramBtn.classList.add("hide");
    }
    else {
        linesTram.classList.remove("hide");
        tramBtn.classList.remove("hide");
        linesTram.classList.add("show");
        tramBtn.classList.add("show");
    }
})

chronoBtn.addEventListener("click", function() {

    if (chronoBtn.classList.contains("show")) {
        linesChrono.classList.remove("show");
        chronoBtn.classList.remove("show");
        linesChrono.classList.add("hide");
        chronoBtn.classList.add("hide");
    }
    else {
        linesChrono.classList.remove("hide");
        chronoBtn.classList.remove("hide");
        linesChrono.classList.add("show");
        chronoBtn.classList.add("show");
    }
})

proxiBtn.addEventListener("click", function() {

    if (proxiBtn.classList.contains("show")) {
        linesProxi.classList.remove("show");
        proxiBtn.classList.remove("show");
        linesProxi.classList.add("hide");
        proxiBtn.classList.add("hide");
    }
    else {
        linesProxi.classList.remove("hide");
        proxiBtn.classList.remove("hide");
        linesProxi.classList.add("show");
        proxiBtn.classList.add("show");
    }
})

flexoBtn.addEventListener("click", function() {

    if (flexoBtn.classList.contains("show")) {
        linesFlexo.classList.remove("show");
        flexoBtn.classList.remove("show");
        linesFlexo.classList.add("hide");
        flexoBtn.classList.add("hide");
    }
    else {
        linesFlexo.classList.remove("hide");
        flexoBtn.classList.remove("hide");
        linesFlexo.classList.add("show");
        flexoBtn.classList.add("show");
    }
})