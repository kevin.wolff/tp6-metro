/// MAP GENERATOR ///

let mymap = L.map('mapidPlan').setView([45.18756, 5.735782], 12); // FIRST LAYER
L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', { // SECOND LAYER
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1Ijoia2V2aW53b2xmZjM4IiwiYSI6ImNrZWExY2VqYzA4ODYyeXQwdzNhMHZhMzgifQ.TR32ViKBTg_E3VduNElxqA'
}).addTo(mymap);

/// HTML LINE TYPE FOR APPENDCHILD ///

let linesTram = document.querySelector(".lines-tram");
let linesChrono = document.querySelector(".lines-chrono");
let linesProxi = document.querySelector(".lines-proxi");
let linesFlexo = document.querySelector(".lines-flexo");

/// REQUETE INFOS ALL LINES ///
$.ajax({
    url: `http://data.metromobilite.fr/api/routers/default/index/routes`,
    dataType: "json",
}).done(function (data) {
    /// GET : TRAM, CHRONO, PROXIMO & FLEXO LINE INFO ///
    for (let i = 0; i < data.length; i++) {
        let info = data[i];

        let linesOnMap = []; // ARRAY LINES DISPLAY

        if (info.type === "TRAM" || info.type === "CHRONO") {

            let lineNumber = document.createElement("div"); // LINE NUMBER
            lineNumber.classList.add("line-number-circle");
            lineNumber.style.backgroundColor = `#${info.color}`;
            lineNumber.innerText = info.shortName;

            if (info.type === "CHRONO") {
              lineNumber.style.color = "#000";
            }

            /// EVENT LISTENER ON LINE NUMBER ///
            lineNumber.addEventListener("click", function () {

                if (linesOnMap.includes(info.id)) {

                    // let index = linesOnMap.indexOf(info.id);// GET INDEX OF ID IN THE ARRAY
                    // linesOnMap.splice(index, 1); // REMOVE ID FROM THE ARRAY
                }
                else {
                    linesOnMap.push(info.id); // PUSH ID IN THE ARRAY

                    /// REQUEST : GEOJSON SELECTED LINE ///
                    $.ajax({
                        url: `http://data.metromobilite.fr/api/lines/json?types=ligne&codes=${info.id}`,
                        dataType: "json",
                    }).done(function (data) {

                        let lineCoordinates = data.features[0].geometry;
                        let lineColor = data.features[0].properties.COULEUR;

                        L.geoJson(lineCoordinates, {
                            color: `rgb(${lineColor})`,
                            weight: 5
                        }).addTo(mymap);

                        /// REQUEST : INFOS SELECTED LINE ///
                        $.ajax({
                            url: `http://data.metromobilite.fr/api/ficheHoraires/json?route=${info.id}`,
                            dataType: "json",
                        }).done(function (data) {

                            /// GET : ALL STOP LOCALISATION OF SELECTED LINE ///
                            for (let o = 0; o < data[0].arrets.length; o++) {

                                let test = L.circle([`${data[0].arrets[o].lat}`, `${data[0].arrets[o].lon}`], {
                                    weight: "8",
                                    color: `rgb(${lineColor})`,
                                    fillColor: `rgb(${lineColor})`,
                                    fillOpacity: "1"
                                }).addTo(mymap); // MARKER FOR EACH STOP
                            }
                        })
                    });
                }
            })

            if (data[i].type === "TRAM") {
                linesTram.appendChild(lineNumber);
            }
            else {
                linesChrono.appendChild(lineNumber);
            }
        }

        else if (info.type === "PROXIMO" || info.type === "FLEXO") {

            let lineNumber = document.createElement("div"); // LINE NUMBER
            lineNumber.classList.add("line-number-square");
            lineNumber.style.backgroundColor = `#${info.color}`;
            lineNumber.innerText = info.shortName;

            /// EVENT LISTENER ON LINE NUMBER ///
            lineNumber.addEventListener("click", function () {

                console.log(linesOnMap); // CONSOLE LOG

                if (linesOnMap.includes(info.id)) {

                    // let index = linesOnMap.indexOf(info.id);// GET INDEX OF ID IN THE ARRAY
                    // linesOnMap.splice(index, 1); // REMOVE ID FROM THE ARRAY
                }
                else {
                    linesOnMap.push(info.id); // PUSH ID IN THE ARRAY

                    /// REQUEST : GEOJSON SELECTED LINE ///
                    $.ajax({
                        url: `http://data.metromobilite.fr/api/lines/json?types=ligne&codes=${info.id}`,
                        dataType: "json",
                    }).done(function (data) {

                        let lineCoordinates = data.features[0].geometry;
                        let lineColor = data.features[0].properties.COULEUR;

                        L.geoJson(lineCoordinates, {
                            color: `rgb(${lineColor})`,
                            weight: 5
                        }).addTo(mymap);

                        /// REQUEST : INFOS SELECTED LINE ///
                        $.ajax({
                            url: `http://data.metromobilite.fr/api/ficheHoraires/json?route=${info.id}`,
                            dataType: "json",
                        }).done(function (data) {
    
                            /// GET : ALL STOP LOCALISATION OF SELECTED LINE ///
                            for (let o = 0; o < data[0].arrets.length; o++) {
    
                                let test = L.circle([`${data[0].arrets[o].lat}`, `${data[0].arrets[o].lon}`], {
                                    weight: "8",
                                    color: `rgb(${lineColor})`,
                                    fillColor: `rgb(${lineColor})`,
                                    fillOpacity: "1"
                                }).addTo(mymap); // MARKER FOR EACH STOP
                            }
                        })
                    });
                }
            })

            if (data[i].type === "PROXIMO") {
                linesProxi.appendChild(lineNumber);
            }
            else {
                linesFlexo.appendChild(lineNumber);
            }
        }
    }
})

/// DISPLAY LINES ///

let tramBtn = document.getElementById("tram-btn");
let chronoBtn = document.getElementById("chrono-btn");
let proxiBtn = document.getElementById("proxi-btn");
let flexoBtn = document.getElementById("flexo-btn");

tramBtn.addEventListener("click", function() {

    if (tramBtn.classList.contains("show")) {
        linesTram.classList.remove("show");
        tramBtn.classList.remove("show");
        linesTram.classList.add("hide");
        tramBtn.classList.add("hide");
    }
    else {
        linesTram.classList.remove("hide");
        tramBtn.classList.remove("hide");
        linesTram.classList.add("show");
        tramBtn.classList.add("show");
    }
})

chronoBtn.addEventListener("click", function() {

    if (chronoBtn.classList.contains("show")) {
        linesChrono.classList.remove("show");
        chronoBtn.classList.remove("show");
        linesChrono.classList.add("hide");
        chronoBtn.classList.add("hide");
    }
    else {
        linesChrono.classList.remove("hide");
        chronoBtn.classList.remove("hide");
        linesChrono.classList.add("show");
        chronoBtn.classList.add("show");
    }
})

proxiBtn.addEventListener("click", function() {

    if (proxiBtn.classList.contains("show")) {
        linesProxi.classList.remove("show");
        proxiBtn.classList.remove("show");
        linesProxi.classList.add("hide");
        proxiBtn.classList.add("hide");
    }
    else {
        linesProxi.classList.remove("hide");
        proxiBtn.classList.remove("hide");
        linesProxi.classList.add("show");
        proxiBtn.classList.add("show");
    }
})

flexoBtn.addEventListener("click", function() {

    if (flexoBtn.classList.contains("show")) {
        linesFlexo.classList.remove("show");
        flexoBtn.classList.remove("show");
        linesFlexo.classList.add("hide");
        flexoBtn.classList.add("hide");
    }
    else {
        linesFlexo.classList.remove("hide");
        flexoBtn.classList.remove("hide");
        linesFlexo.classList.add("show");
        flexoBtn.classList.add("show");
    }
})