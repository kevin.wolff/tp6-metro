# TP6 - Mobilite Grenoble

**A rendre pour le 04/09/2020 au soir**

### Webpack config :

- webpack & webpack-cli ^4.44.1, ^3.3.12
- css-loader ^4.2.2
- mini-css-extract-plugin ^0.11.0
- node-sass ^4.14.1
- sass-loader ^10.0.1
- style-loader ^1.2.1

### Consignes :

[Resetage](https://docs.google.com/spreadsheets/d/1m2XTkS181Sg2CxFKk68H9rwKUVBmmc5Z1QG6hFI1saY/edit?usp=sharing)

[maquette + prototype](https://www.figma.com/file/H0MN35bPMOMKoMXOByQnwz/Untitled?node-id=0%3A1)

Vous devez réaliser un site internet responsive dont l’objectif est de pouvoir consulter les horaires de toutes les lignes des transports en commun de l’agglomération Grenobloise. L'application doit être mobile first.

[Vous utiliserez l’API de la métro](https://www.mobilites-m.fr/pages/opendata/OpenDataApi.html)

Ce service devra comporter à minima les spécifications suivantes :

- Une liste de toutes les lignes existantes avec leurs codes couleurs et leur informations respectives (numéro de ligne, destination, code couleur etc)
- Chaque catégorie de transport doit avoir son icône dédiée (bus/tram)
- Le détail de chaque ligne qui contiendra toutes les informations utiles (horaires, arrêts etc)
- Une carte interactive qui présentera les informations suivantes :
  - Tracé des lignes
  - Bonus : les différents arrêts de la ligne
- Vous devez avoir un système pour enregistrer vos lignes favorites (localstorage suffisant)
- Un système d'annonces pour informer les usagers des règles sanitaires en vigueur : ce système devra se lancer au chargement de la page en tant que bandeau haut. Il pourrat être fermé mais il devra se ré-ouvrir au bout de quelques minutes

**Points obligatoires :**

- Framework CSS interdit
- SASS
- JQuery obligatoire avec Ajax mais aucun framework additionnel
- Leaflet pour les cartes
- Test de contrastes de couleurs
- Utiliser un système de recettage pour enlever les bugs